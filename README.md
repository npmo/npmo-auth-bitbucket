# npmo-auth-bitbucket

> Integrate Bitbucket auth in your npm On-Site instance

An authentication and authorization plugin for npm On-Site backed by bitbucket.org.

More docs coming soon.
